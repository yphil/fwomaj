# Fwomaj

[Fromaj est un petit lecteur multimedia](http://linuxfr.org/news/fwomaj-0-3-videos-a-la-coupe-au-rayon-frais) qui permet de visionner/éditer ("dérusher") rapidement des fichiers videos pour un montage ultérieur.
Modification du taux images/seconde, découpage du début et de la fin, changement de container, etc.

Fwomaj is a small media player to quickly view and edit video files (it's called "derushing") for later editing.
Change framerate and other encoding parameters, trim the start and end, etc.

![screenshot](https://bitbucket.org/yphil/fwomaj/raw/master/screenshot.jpg)

## Usage
Fwomaj is a media player that can also set and save the following parameters to a new video file:

- Start time
- End time
- Framerate
- Codec container

Move the time sliders to set the start and end of the saved movie ; Use the Encoding menu to select framerate, and  container. Upon clicking the Save (disk icon) button, the trimmed and encoded file is saved in the original file directory.

## Install
See [build-install.sh](https://bitbucket.org/yphil/fwomaj/src/master/build-install.sh) or [download the latest package](https://bitbucket.org/yphil/fwomaj/downloads/).


Icon by [Buuf](http://mattahan.deviantart.com)

Philip Yassin [WWW](http://yphil.bitbucket.io) | Help me make it [Patreon](https://www.patreon.com/yassinphilip)
