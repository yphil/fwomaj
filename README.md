# Fwomaj

[Fromaj est un petit lecteur multimedia](http://linuxfr.org/news/fwomaj-0-3-videos-a-la-coupe-au-rayon-frais) qui permet de visionner/éditer ("dérusher") rapidement des fichiers videos pour un montage ultérieur.
Modification du taux images/seconde, découpage du début et de la fin, changement de container, etc.

Fwomaj is a small media player to quickly view and edit video files (it's called "derushing") for later editing.
Change framerate and other encoding parameters, trim the start and end, etc.

![screenshot](https://framagit.org/yphil/Fwomaj/raw/master/screenshot.jpg)

## Usage
Fwomaj can set and save the following parameters to a *new* (in the current directory, named `FILE_NAME_START-END_FRAMERATE.EXTENSION` ie `my_huge_video.mp4_0:02:45-0:02:52_60fps.mkv`) video *file*:

- Start time
- End time
- Framerate
- Codec container

Move the **time sliders** to set the *START* and *END* of the new movie clip ; Use the **Encoding** menu to select the **framerate** (default *30fps*) and  **container** (default *Matroska*). Upon clicking the **Save** (disk icon) button, the trimmed and encoded file is saved in the original file directory.

## Install
Run [build-install.sh](https://framagit.org/yphil/Fwomaj/blob/master/build-install.sh) or [download the latest package](https://framagit.org/yphil/Fwomaj/-/archive/master/Fwomaj-master.zip).


Icon by [Buuf](http://mattahan.deviantart.com)

Brought to you with 💚 by [Yassin Philip](http://yphil.bitbucket.io) | Help me make it 🍎  [Donate](https://liberapay.com/yPhil/donate)
