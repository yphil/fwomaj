#!/usr/bin/env bash

VERSION=$(head -1 debian/changelog | grep -oP "(?<=\().*(?=\))")

if debuild -I -us -uc; then

    read -e -p "
#### Install Fwomaj (fwomaj_${VERSION}_all.deb)? [Y/n] " YN

    if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then
        sudo apt install gdebi
        sudo gdebi ../fwomaj_${VERSION}_all.deb
    fi

    read -e -p "
#### Upload Fwomaj binaries to bitbucket repo download section? [y/N] " YN

    if [[ $YN == "y" || $YN == "Y" ]] ; then
        curl --progress-bar -o upload.log -u yassinphilip -X POST https://api.bitbucket.org/2.0/repositories/yassinphilip/fwomaj/downloads --progress-bar -F files=@../fwomaj_${VERSION}_all.deb -F files=@../fwomaj_${VERSION}.tar.gz && rm -f upload.log

    fi

    read -e -p "
#### Cleanup Fwomaj package files? [Y/n] " YN
    if [[ $YN == "y" || $YN == "Y" || $YN == "" ]] ; then
        rm -fv ../fwomaj_${VERSION}*
    fi

fi
