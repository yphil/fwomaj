#!/usr/bin/env python3

import sys, os, glob, platform, tempfile, time, argparse
import subprocess as sp
from datetime import datetime, timedelta
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
gi.require_version('GstVideo', '1.0')
from gi.repository import GstVideo
gi.require_version('GstPbutils', '1.0')
from gi.repository import GstPbutils
from gi.repository import GObject, Gdk, Gio, GLib, GdkX11
from gi.repository import GdkPixbuf

GObject.threads_init()
Gst.init(None)

__version__ = '0.3.3'

parser = argparse.ArgumentParser(description='Fwomaj is a simple media player/editor', prog='Fwomaj')
parser.add_argument('filename', nargs='?')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {version}'.format(version=__version__))
args = parser.parse_args()

uri = '' if (args.filename is None) else 'file://{}'.format(os.path.abspath(args.filename))

FFMPEG = 'ffmpeg' if (platform.system() == 'Linux') else 'ffmpeg.exe'

LICENSE_SUMMARY = """
Fwomaj is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 2.

This program is distributed in the hope that it will be useful,
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA)
"""

AUTHORS = "Yassin Philip https://yphil.bitbucket.io (Main dev)", "Fabien Moreau https://bitbucket.org/fabienvm/ (Wayland integration)"


MENU_ITEMS = '''
      <menu action="File">
        <menuitem action="Open"/>
        <menuitem action="Info"/>
        <menuitem action="Quit"/>
      </menu>
      <menu action="Encoding">
        <menu action='FileFormat'>
          <menuitem action="mkv"/>
          <menuitem action="mp4"/>
          <menuitem action="avi"/>
        </menu>
        <menu action="FrameRate">
          <menuitem action="60"/>
          <menuitem action="30"/>
          <menuitem action="29.97"/>
          <menuitem action="25"/>
          <menuitem action="24"/>
        </menu>
      </menu>
      <separator/>
      <menu action="Help" name="AboutMenu">
        <menuitem action="About"/>
      </menu>'''

MENU = '''<ui>
    <menubar name="MenuBar">
    ''' + MENU_ITEMS + '''
    </menubar>
    <popup name="PopUp">
    ''' + MENU_ITEMS + '''
    </popup>
    </ui>'''

class WForm(object):

    @classmethod
    def execute(cls, filename, wbox, style_provider, spinner, label):
        """
        Generate the media file audio track(s) wafeworm image using FFMPEG, display it, inform user of the process.
        """

        global FFMPEG

        label.set_text(' Generating WaveForm image for {}... '.format(os.path.basename(filename)))
        unique_file = os.path.join(tempfile.gettempdir(), 'fwomaj-{}.png'.format(time.time()))

        command = [FFMPEG, '-y', '-i', filename,
                                 '-filter_complex', '[0:a]aformat=channel_layouts=mono,showwavespic=s=600x200,crop=600:40:0:80',
                                 '-frames:v', '1', unique_file]

        p = sp.Popen(command, stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE)

        def p_callback(p):
            p.poll()
            if p.returncode is None:
                spinner.start()
                return True
            else:
                spinner.stop()
                label.set_text('')

                if os.path.isfile(unique_file):
                    CSS_ALT = '#WBox {{background-size:100% 100%;background-image: url("{uri}");}}'.format(uri=unique_file)
                else:
                    CSS_ALT = '#WBox {background-size:100% 100%}'

                style_provider.load_from_data(CSS_ALT.encode())
                return False

        GObject.timeout_add(1000, p_callback, p)


class Executor(object):

    @classmethod
    def execute(cls, command, statusbar, spinner, out_file):
        """
        Execute a given command and inform the user of the progress.
        """

        s1 = time.strftime("%H:%M:%S")
        FMT = '%H:%M:%S'

        p = sp.Popen(command, stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE, universal_newlines=True)

        def p_callback(p):
            p.poll()
            if p.returncode is None:
                spinner.start()
                statusbar.push(0, 'Encoding {}...'.format(os.path.basename(out_file)))
                return True
            else:
                spinner.stop()
                tdelta = datetime.strptime(time.strftime("%H:%M:%S"), FMT) - datetime.strptime(s1, FMT)
                if tdelta.days < 0:
                    tdelta = timedelta(days=0,
                                       seconds=tdelta.seconds, microseconds=tdelta.microseconds)
                statusbar.set_tooltip_markup('Encoded {} in {}'.format(out_file[:7], tdelta))
                statusbar.push(0, 'Encoded {} in {}'.format(os.path.basename(out_file), tdelta))
                return False

        GObject.timeout_add(1000, p_callback, p)

class Fwomaj(Gtk.Window):
    def __init__(self):
        self.__is_fullscreen = False
        Gtk.Window.__init__(self, title="Fwomaj")
        self.set_default_size(600, 450)
        self.win = self
        self.is_playing = False
        self.temp_height = 0
        self.temp_width = 0
        self.file_format = 'mkv'
        self.frame_rate = '30'

        self.loaded_file = uri

        self.style_provider = Gtk.CssProvider()

        self.waveform_box = Gtk.Box()
        self.waveform_box.set_name('WBox')
        self.waveform_box.set_size_request(600, 40)
        self.label_no_wform = Gtk.Label(xalign=0)

        self.statusbar = Gtk.Statusbar()
        self.statusbar.set_name('StatusBar')

        self.spinner_encode = Gtk.Spinner()
        self.spinner_wform = Gtk.Spinner()

        top_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        self.button_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)

        self.play_button_image_play = Gtk.Image()
        self.play_button_image_play.set_from_stock("gtk-media-play", Gtk.IconSize.BUTTON)

        self.play_button_image_pause = Gtk.Image()
        self.play_button_image_pause.set_from_stock("gtk-media-pause", Gtk.IconSize.BUTTON)

        self.trim_button_image = Gtk.Image()
        self.trim_button_image.set_from_stock("gtk-floppy", Gtk.IconSize.BUTTON)

        self.play_button = Gtk.Button()
        self.play_button.set_image(self.play_button_image_pause)

        self.trim_button = Gtk.Button()

        self.video_box = Gtk.EventBox()
        self.video_box.set_size_request(400, 250)

        # Sliders
        self.slider_from_label = Gtk.Label()
        self.slider_from_label.set_alignment(xalign=1, yalign=0.5)

        self.adj1 = Gtk.Adjustment(0.0, 0.0, 0.0, 0.1, 1.0, 1.0)
        self.adj2 = Gtk.Adjustment(0.0, 0.0, 0.0, 0.1, 1.0, 1.0)
        self.adj3 = Gtk.Adjustment(0.0, 0.0, 0.0, 0.1, 1.0, 1.0)

        self.slider_play = Gtk.HScale(orientation=Gtk.Orientation.HORIZONTAL, adjustment=self.adj1)
        self.slider_play.set_increments(1, 10)
        self.slider_play_handler_id = self.slider_play.connect('value-changed', self.on_slider_change)
        self.slider_play.set_draw_value(False)

        self.slider_from = Gtk.HScale(orientation=Gtk.Orientation.HORIZONTAL, adjustment=self.adj2)
        self.slider_from.set_increments(100, 100)
        self.slider_from.set_draw_value(False)

        self.slider_to = Gtk.HScale(orientation=Gtk.Orientation.HORIZONTAL, adjustment=self.adj3)
        self.slider_to.set_increments(100, 100)
        self.slider_to.set_draw_value(False)

        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            self.style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

        # MENUS
        uimanager = self.create_ui_manager()
        ag_father = Gtk.ActionGroup('father')
        ag_son = Gtk.ActionGroup('son')

        ag_father.add_actions([('File', None, '_File'),
                                  ('Sound', None, '_Sound'),
                                  ('Encoding', None, '_Encoding'),
                                  ('FrameRate', None, '_Frame Rate'),
                                  ('Help', None, '_Help')])

        ag_father.add_actions([('Open', Gtk.STOCK_OPEN, '_Open File', '<Control>o',
                                  'Info', self.on_file_choose)])

        ag_father.add_actions([('Info', Gtk.STOCK_INFO, '_File Info', '<Control>i',
                                  'Info', self.on_file_info)])

        ag_father.add_actions([('Quit', Gtk.STOCK_QUIT, '_Quit', None,
                                  'Quit the Program', self.on_quit)])

        ag_father.add_actions([('About', Gtk.STOCK_ABOUT, '_About', None,
                                  'About Fwomaj', self.about_box)])

        action_file_format = Gtk.Action("FileFormat", "_File Format", "Create a new file", Gtk.STOCK_SAVE_AS)
        ag_father.add_action(action_file_format)

        ag_father.add_radio_actions([('mkv', None, '_MKV', '<Control>k', 'Matroska', 1),
                                       ('mp4', None, '_MP4', '<Control>4', 'MP4', 2),
                                       ('avi', None, '_AVI', '<Control>a', 'AVI', 3),
                                       ], 0, self.mediaformat_cb)

        ag_father.add_radio_actions([('60', None, '_60', None, '60', 1),
                                       ('30', None, '_30', None, '30', 2),
                                       ('29.97', None, '_29.97', None, '29.97', 3),
                                       ('25', None, '_25', None, '25', 4),
                                       ('24', None, '_24', None, '24', 5),
                                       ], 2, self.framerate_cb)

        uimanager.insert_action_group(ag_father, 1)

        uimanager.insert_action_group(ag_son)

        menubar = uimanager.get_widget("/MenuBar")

        self.video_box.connect("button-press-event", self.on_click_event)

        # Callbacks
        self.win.connect("key-press-event", self.on_kb_event)
        self.win.connect("window-state-event", self.on_window_state_event)
        self.win.connect('button-press-event', self.on_click_event)

        self.win.connect("delete-event",self.on_quit)

        self.video_box.connect("motion_notify_event", self.on_win_mouse_move_event)

        self.adj2.connect('value_changed', self.on_adjustment_changed)
        self.adj3.connect('value_changed', self.on_adjustment_changed)
        self.play_button.connect("clicked", self.toggle_play)

        self.video_box.add_events(self.video_box.get_events()
                                  | Gdk.EventMask.BUTTON_PRESS_MASK       # mouse down
                                  | Gdk.EventMask.BUTTON_RELEASE_MASK   # mouse up
                                  | Gdk.EventMask.POINTER_MOTION_MASK)   # mouse move


        self.popup = uimanager.get_widget("/PopUp")

        top_box.pack_start(menubar, False, False, 0)
        top_box.pack_start(self.video_box, True, True, 0)
        main_box.pack_start(top_box, True, True, 0)
        main_box.add(self.waveform_box)
        self.add(main_box)

        self.button_box.pack_start(self.play_button, True, True, 0)
        self.button_box.pack_start(self.trim_button, False, False, 0)

        self.trim_button.set_image(self.trim_button_image)
        self.trim_button.set_always_show_image(True)

        self.statusbar.pack_start(self.spinner_encode, False, False, 0)

        self.waveform_box.pack_start(self.label_no_wform, True, True, 0)
        self.waveform_box.pack_start(self.spinner_wform, False, False, 4)

        main_box.add(self.slider_play)
        main_box.add(self.button_box)
        main_box.add(self.slider_from)
        main_box.add(self.slider_to)
        main_box.add(self.statusbar)
        self.show_all()

        # GStreamer
        self.pipeline = Gst.Pipeline()

        self.bus = self.pipeline.get_bus()
        self.bus.add_signal_watch()
        self.bus.connect('message::eos', self.on_eos)
        self.bus.connect('message::error', self.on_error)
        self.bus.connect("message::state-changed", self.on_state_changed)

        self.playbin = Gst.ElementFactory.make('playbin', None)
        gtksink = Gst.ElementFactory.make("gtksink", "videobox")

        self.playbin.set_property("video-sink",gtksink)
        widget = gtksink.get_property("widget")
        self.video_box.add(widget)
        widget.show()
        self.pipeline.add(self.playbin)


        self.loaded_file = uri

        if self.loaded_file is not '':
            self.on_load_file(self, self.loaded_file)

    def on_state_changed(self, bus, msg):
        """
        GST bus messages callback
        """

        old, new, pending = msg.parse_state_changed()

        if not msg.src == self.playbin:
            return

        self.state = new
        self.play_button.set_image(self.play_button_image_pause)

        if new == Gst.State.PAUSED:
            self.play_button.set_image(self.play_button_image_play)

    def on_kb_event(self, widget, ev):
        """
        Key press events on the main window callback.
        """

        if ev.keyval == Gdk.KEY_space:
            self.toggle_play(widget)
            return True # event has been handled

        if Gdk.keyval_name(ev.keyval) == "e":
            self.slider_to.set_value(self.slider_play.get_value()*1000 )
            return True

        if Gdk.keyval_name(ev.keyval) == "s":
            self.slider_from.set_value(self.slider_play.get_value() *1000)
            return True

        if Gdk.keyval_name(ev.keyval) == "f" or ev.keyval == Gdk.KEY_Escape:
            self.fullscreen_mode()
            return True

    def on_click_event(self, widget, event):
        """
        Mouse buttons callback.
        """

        if event.type == Gdk.EventType.BUTTON_PRESS and event.button == 3:
            self.popup.popup(None, None, None, None, event.button, event.time)

        if event.type == Gdk.EventType.DOUBLE_BUTTON_PRESS and isinstance(widget, Gtk.EventBox):
            self.fullscreen_mode()

        return True

    def toggle_play(self, button):
        """
        Toggle play/pause.
        """

        if self.is_playing:
            self.pipeline.set_state(Gst.State.PAUSED)
            self.is_playing = False
        else:
            self.pipeline.set_state(Gst.State.PLAYING)
            GLib.timeout_add(1000, self.update_slider)
            self.is_playing = True

    def on_file_choose(self, widget):
        """
        File chooser dialog callback
        """

        dialog = Gtk.FileChooserDialog("Please choose a file", self,
            Gtk.FileChooserAction.OPEN,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        self.add_filters(dialog)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.on_load_file(self, 'file://{}'.format(dialog.get_filename()))
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()

    def add_filters(self, dialog):
        """
        Add file name filters to the calling "file chooser" dialog.
        """

        containers = {
            "Matroska": "video/x-matroska",
            "MPEG-4": "video/mp4",
            "3GP": "video/3gp",
            "A/V Interleave": "video/x-msvideo",
            "Quicktime": "video/quicktime",
            "All files": "*"
        }

        for container in containers:
            container_filter = Gtk.FileFilter()
            container_filter.set_name(container)
            if container is "All files":
                container_filter.add_pattern("*")
            else:
                container_filter.add_mime_type(containers[container])
            dialog.add_filter(container_filter)

    def on_load_file(self, widget, filename):
        """
        Load a file, either at startup or later.
        """

        WForm.execute(filename, self.waveform_box, self.style_provider, self.spinner_wform, self.label_no_wform)

        self.loaded_file = filename

        duration = get_info(filename, 'milliseconds')

        self.adj1.set_upper(duration)
        self.adj2.set_upper(duration)
        self.adj3.set_upper(duration)
        self.slider_from.set_range(0, duration)
        self.slider_play.set_range(0, duration)
        self.slider_to.set_range(0, duration)
        self.slider_to.set_value(duration)
        self.slider_from_label.set_label('Time: 00:00:00 / {}'.format(get_info(filename, 'hh:mm:ss').strip()))

        self.trim_button.set_label(' from {} to {}'.format(convert_time(self.slider_from.get_value(), 'hh:mm:ss.ms'), convert_time(self.slider_to.get_value(), 'hh:mm:ss.ms')))
        self.trim_button.connect('clicked', self.on_trim_button_clicked, filename)

        self.set_title('{} - Fwomaj'.format(os.path.basename(filename)))
        self.statusbar.push(0, '{}'.format(os.path.basename(filename)))

        self.pipeline.set_state(Gst.State.NULL)
        GLib.timeout_add(1000, self.update_slider)
        self.playbin.set_property('uri', filename)
        self.pipeline.set_state(Gst.State.PLAYING)
        self.play_button.set_image(self.play_button_image_pause)
        self.is_playing = True

    def fullscreen_mode(self):
        """
        Toggle pseudo-FullScreen mode.
        Called from the on_kb_event method.
        """

        if self.__is_fullscreen:
            self.win.unfullscreen()

            self.slider_play.show()
            self.button_box.show()
            self.slider_from.show()
            self.slider_to.show()
            self.trim_button.show()
            self.statusbar.show()
            self.waveform_box.show()
        else:
            self.win.fullscreen()

            self.slider_play.hide()
            self.button_box.hide()
            self.slider_from.hide()
            self.slider_to.hide()
            self.trim_button.hide()
            self.statusbar.hide()
            self.waveform_box.hide()

    def on_win_mouse_move_event(self, widget, ev):
        """
        Handle mouse move events on the main window. TODO: Handle mouse timeouts (blanking)
        """

        if self.__is_fullscreen:
            self.slider_play.show()
            self.button_box.show()

    def on_window_state_event(self, widget, ev):
        """
        Detect window state events to determine fullscreen state
        """

        self.__is_fullscreen = bool(ev.new_window_state & Gdk.WindowState.FULLSCREEN)

    def mediaformat_cb(self, action, current):
        """
        Medi format menu callback.
        """

        self.file_format = current.get_name()
        return

    def framerate_cb(self, action, current):
        """
        Framerate menu callback.
        """

        self.frame_rate = current.get_name()
        return

    def on_trim_button_clicked(self, button, filename):
        """
        Trim button callback.
        """

        self.execute(filename, convert_time(self.slider_from.get_value(), 'hh:mm:ss.ms'), convert_time(self.slider_to.get_value(), 'hh:mm:ss.ms'))

    def execute(self, filename, start_time, end_time):
        """
        Run ffmpeg jobs. Construct the ffmpeg command according to user choices.
        """

        global FFMPEG

        out_file = '{}_{}-{}_{}fps.{}'.format(filename,
                                              str(start_time),
                                              str(end_time),
                                              self.frame_rate,
                                              self.file_format)

        command = [FFMPEG, '-y',
                        '-i', filename,
                        '-r', self.frame_rate,
                        '-strict', '-2',
                        '-c:v', 'libx264',
                        '-ss', start_time,
                        '-to', end_time,
                        out_file]

        Executor.execute(command, self.statusbar, self.spinner_encode, out_file)

    def on_adjustment_changed(self, widget):
        """
        Trim sliders adjustment callback
        """

        self.trim_button.set_label(' from {} to {}'.format(convert_time(self.slider_from.get_value(), 'hh:mm:ss.ms'), convert_time(self.slider_to.get_value(), 'hh:mm:ss.ms')))

    def on_slider_change(self, widget):
        """
        Both "Trim" sliders callback
        """

        seek_time_secs = self.slider_play.get_value()
        self.playbin.seek_simple(Gst.Format.TIME,  Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT, seek_time_secs * Gst.SECOND)

    #called periodically by the Glib timer, returns false to stop the timer
    def update_slider(self):
        """
        This is going to be refactored to be a direct callback from the pipe events.
        """

        if not self.is_playing:
            return False # cancel timeout

        else:
            success, self.duration = self.playbin.query_duration(Gst.Format.TIME)
            if not success:
                raise GenericException("Couldn't fetch song duration")
            else:
                self.slider_play.set_range(0, self.duration / Gst.SECOND)
            success, position = self.playbin.query_position(Gst.Format.TIME)
            if not success:
                raise GenericException("Couldn't fetch current song position to update slider")

            # block seek handler so we don't seek when we set_value()
            self.slider_play.handler_block(self.slider_play_handler_id)
            self.slider_play.set_value(float(position) / Gst.SECOND)
            self.slider_play.handler_unblock(self.slider_play_handler_id)

        return True # continue calling every x milliseconds

    def create_ui_manager(self):
        """
        Store the MENU xml structure and add its elements to the accel group.
        """

        uimanager = Gtk.UIManager()
        uimanager.add_ui_from_string(MENU)
        accelgroup = uimanager.get_accel_group()
        self.add_accel_group(accelgroup)
        return uimanager

    def on_quit(self, widget, ev=None):
        """
        Destroy the main window and cleanup/delete the waveform image files in the temp dir.
        """

        for f in glob.glob(os.path.join(tempfile.gettempdir(), 'fwomaj-{}.png'.format('*'))):
            print('Removing temp file {}'.format(f))
            os.remove(f)
        Gtk.main_quit()

    def about_box(self, widget, data=None):
        """
        About dialog.
        """

        about = Gtk.AboutDialog()
        about.set_transient_for(self.win)
        about.set_program_name('Fwomaj')
        about.set_version(__version__)
        about.set_copyright('(c) Philip \'xaccrocheur\' Yassin')
        about.set_comments('Simple media player/[au|e]ditor')
        about.set_website('https://bitbucket.org/yassinphilip/fwomaj')

        fwomaj_icon = '/usr/share/icons/hicolor/128x128/apps/fwomaj.png'

        if os.path.isfile(fwomaj_icon):
            about.set_logo(GdkPixbuf.Pixbuf.new_from_file(fwomaj_icon))

        about.set_license(LICENSE_SUMMARY)
        about.set_authors(AUTHORS)
        about.run()
        about.destroy()

    def run(self):
        """
        Main loop, feeds images from the pipe to the video_box.
        """
        self.pipeline.set_state(Gst.State.PLAYING)
        GLib.timeout_add(1000, self.update_slider)
        Gtk.main()

    def on_eos(self, bus, msg):
        """
        GST bus messages callback
        """

        self.pipeline.seek_simple(
            Gst.Format.TIME,
            Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT,
            0
        )

    def on_error(self, bus, msg):
        """
        GST bus messages callback
        """

        print('on_error():', msg.parse_error())

    def on_file_info(self, widget):
        """
        File info menu callback. A GTK message window.
        """

        f = self.loaded_file
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO,
                                   Gtk.ButtonsType.OK, str(os.path.basename(f)))

        dialog.set_default_size(500, 800)
        box = dialog.get_message_area()
        minfo = get_info(f, 'all')

        liststore = Gtk.ListStore(str, str)
        treeview = Gtk.TreeView(model=liststore)

        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        scrolledwindow.set_size_request(500, 800)

        renderer_1 = Gtk.CellRendererText()
        renderer_2 = Gtk.CellRendererText()
        column_1 = Gtk.TreeViewColumn("Parameter", renderer_1, text=0)
        column_2 = Gtk.TreeViewColumn("Value", renderer_2, text=1)

        treeview.append_column(column_1)
        treeview.append_column(column_2)

        for tag in get_info(f, 'all').split('\n'):
            line = tag.partition(":")
            liststore.append([line[0], line[2][:50]])

        scrolledwindow.add_with_viewport(treeview)

        box.add(scrolledwindow)

        box.show_all()
        dialog.run()
        dialog.destroy()

def get_info(filename, time_fmt):
    """
    A generic function to extract metadata from the media file.
    MediaInfo courtesy of Zenitram :)
    """

    if time_fmt == 'milliseconds':
        mediainfo_options = 'General;%Duration%'
    elif time_fmt == 'hh:mm:ss.ms' or time_fmt == 'hh:mm:ss':
        mediainfo_options = 'General;%Duration/String3%'
    elif time_fmt == 'framerate':
        mediainfo_options = 'General;%FrameRate%'
    elif time_fmt == 'all':
        mediainfo_options = 'General'
    else:
        mediainfo_options = 'General;%Duration/String1%'

    p = sp.Popen(['mediainfo', '--inform=' + mediainfo_options, filename], stdout=sp.PIPE, stderr = sp.STDOUT)

    if time_fmt == 'milliseconds':
        return float(p.communicate()[0])
    elif time_fmt == 'hh:mm:ss':
        return p.communicate()[0][:8]
    else:
        return str(p.communicate()[0].decode())


def convert_time(time_string, time_fmt):
    """
    Time conversions.
    """

    if time_fmt == 'hh:mm:ss.ms':
        m, s = divmod(time_string/1000, 60)
        h, m = divmod(m, 60)
        return "%d:%02d:%02d" % (h, m, s)
    elif time_fmt == 'milliseconds':
        l = time_string.split(':')
        return int(l[0]) * 3600 + int(l[1]) * 60 + int(l[2])
    else:
        m, s = divmod(time_string/1000000000, 60)
        h, m = divmod(m, 60)
        return "%d:%02d:%02d" % (h, m, s)

win = Fwomaj()
win.show_all()
win.connect("delete-event", Gtk.main_quit)
win.run()
