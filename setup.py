from distutils.core import setup

setup(name = "Fwomaj",
      description = "A cheesy media player/editor",
      author = "Yassin Philip",
      author_email = "yassinphil@gmail.com",
      url = "https://bitbucket.org/yassinphilip/fwomaj",
      license='GPLv2',
      scripts=['fwomaj'],
      data_files = [("share/icons/hicolor/16x16/apps", ["icon/16/fwomaj.png"]),
                    ("share/icons/hicolor/22x22/apps", ["icon/22/fwomaj.png"]),
                    ("share/icons/hicolor/24x24/apps", ["icon/24/fwomaj.png"]),
                    ("share/icons/hicolor/32x32/apps", ["icon/32/fwomaj.png"]),
                    ("share/icons/hicolor/48x48/apps", ["icon/48/fwomaj.png"]),
                    ("share/icons/hicolor/64x64/apps", ["icon/64/fwomaj.png"]),
                    ("share/icons/hicolor/72x72/apps", ["icon/72/fwomaj.png"]),
                    ("share/icons/hicolor/96x96/apps", ["icon/96/fwomaj.png"]),
                    ("share/icons/hicolor/128x128/apps", ["icon/128/fwomaj.png"]),
                    ("share/applications", ["fwomaj.desktop"]) ] )
